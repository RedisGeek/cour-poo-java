package org.dppoo.orm;

import javax.persistence.*;

@Entity
@Table(name = "eleve")

public class EntityExample {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Column(name = "eleve_nom")
    private String nom;

    @Column(name = "eleve_classe")
    private String classe;

    @Column(name = "eleve_age")
    private int age;

    public EntityExample(){

    }

    @Override
    public String toString() {
        return "EntityExample{" +
                "Id=" + Id +
                ", nom='" + nom + '\'' +
                ", classe='" + classe + '\'' +
                ", age=" + age +
                '}';
    }


    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getClasse() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
