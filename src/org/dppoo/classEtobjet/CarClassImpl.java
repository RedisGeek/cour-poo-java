package org.dppoo.classEtobjet;

import java.util.Scanner;

public class CarClassImpl {
    public static void main(String[] args) {

        Carclass car = new Carclass();
        Scanner sc = new Scanner(System.in);

        System.out.println("entrer le numero de serie : ");
        int num = sc.nextInt();
        System.out.println("Entrer la marque : ");
        String mark = sc.next();

        car.setNumSerie(num);
        car.setMarque(mark);

        System.out.println("Numero de Serie : "+car.getNumSerie()+" Marque : "+car.getMarque());
        System.out.println(car.getClass());
    }

}
