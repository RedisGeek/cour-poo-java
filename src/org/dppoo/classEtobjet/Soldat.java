package org.dppoo.classEtobjet;

public class Soldat {

    private int vie;
    private int balle;
    private boolean isLive;

    /*
    * Constructeur vide , initialise
    * */
    public Soldat(){

    }

    /*
    * Accesseur & Mutateur
    * */

    public int getVie() {
        return vie;
    }

    public int getBalle() {
        return balle;
    }

    public boolean isLive() {
        return isLive;
    }

    /*
    * Manipulateur
    * */

    public void setVie(int vie) {
        this.vie = vie;
    }

    public void setBalle(int balle) {
        this.balle = balle;
    }

    public void setLive(boolean live) {
        isLive = live;
    }

    /*
    * Methode de classe
    * */
    void Attaquer(){
        int lifeRest = this.vie - 1;
        int balleRest = this.balle - 1;
        System.out.println("A l attaque !!! VIE : "+lifeRest+" BALLE : "+balleRest);
    }

    void Coup(){
        int vieRest = this.getVie();
        if (vieRest == 0){
            this.setLive(false);
            System.out.println("T es mort !!!");
        } else {
            this.setLive(true);
            System.out.println("Encore en vie !!");
        }
        System.out.println("Boom !!! ");
    }
}
