package org.dppoo.classEtobjet;

public class Carclass {

    /*
    * Attributs
    * */
    private int numSerie;
    private String marque;

    /*
    * Constructeur vide
    * */
    public Carclass(){

    }

    /*
    * accesseur , getters & setters
    * */
    public int getNumSerie(){
        return numSerie;
    }
    public String getMarque(){
        return marque;
    }

    /*
    * Manipulateur
    * */
    public void setNumSerie(int serie) {
        this.numSerie = serie;
    }
    public void setMarque(String mark) {
        this.marque = mark;
    }

    /*
    * Methode de Classe
    * */
    void Demarrer(){
        System.out.print("Voiture demarrer");
    }

    void Stopper(){
        System.out.print("STOP !!!");
    }
}
