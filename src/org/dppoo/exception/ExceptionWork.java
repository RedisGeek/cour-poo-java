package org.dppoo.exception;

import java.util.Scanner;

public class ExceptionWork {

    /*
    * Les exception en java
    * Mot cle : try ... catch
    *   throws / throw
    *   finally
    * */

    public static void main(String[] args) {
        /*
        * Exemple exception
        * */

        /*int x = 20;
        int y = 0;

        double j = x / y;
        System.out.println(j);*/

        /*
        * Mi-generer exception de type : ArithmeticException
        * */

        /*
        * THROWS : mi-signaler an JVM hoe misy methode na classe na objet mety hapisy erreur fa mila ampiasana bloc try ... catch
        * THROW : mi-lever exception fotsiny manuellement , mi-instance objet de type Exception (objet herite) ex: throw new TsisyAnaranaException
        * */



    }
}
