package org.dppoo.constructeur;

public class Ville {

    /*
    * Variable d instance
    * Pour acceder au variable d instance :
    *   - GET (getters) :
    * Pour Ecriture , Modification :
    *   - SET (setters) :
    * */
    private int nbrHabitant;
    private String nomVille;
    private String nomPays;

    /*
    * Constructeur sans parametre
    *
    * */
    public Ville(){

    }

    /*
    * Constructeur avec parametre
    * */
   /* public Ville(int habitant , String nom , String pays){
        nbrHabitant = habitant;
        nomVille = nom;
        nomPays = pays;
    }*/

    /*
    * Accesseur
    * */
    public int getNbrHabitant() {
        return nbrHabitant;
    }
    public String getNomVille() {
        return nomVille;
    }
    public String getNomPays() {
        return nomPays;
    }

    /*
    * Mutateur
    * Manipulateur
    * */
    public void setNbrHabitant(int nbrHabitant) {
        this.nbrHabitant = nbrHabitant;
    }
    public void setNomVille(String nomVille) {
        this.nomVille = nomVille;
    }
    public void setNomPays(String nomPays) {
        this.nomPays = nomPays;
    }
}
