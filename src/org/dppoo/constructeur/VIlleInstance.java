package org.dppoo.constructeur;

public class VIlleInstance {
    public static void main(String[] args) {
        Ville tananarive = new Ville();


        tananarive.setNbrHabitant(1500);
        tananarive.setNomVille("ANTANANARIVO");
        tananarive.setNomPays("MADAGASCAR");

        int newNbrHabitant = tananarive.getNbrHabitant();
        String nomVIlle = tananarive.getNomVille();
        String nomPays = tananarive.getNomPays();


        System.out.println("Nombre d habitant : "+newNbrHabitant);
        System.out.println("Nom Ville : "+nomVIlle);
        System.out.println("Nom Pays : "+nomPays);
    }
}
