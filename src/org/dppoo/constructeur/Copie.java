package org.dppoo.constructeur;

public class Copie {

    public int age;
    public String nom;

    /*
    * Constructeur de copie
    * Initialise un instance avec une copie de meme classe
    * */
    // NOM_CLASSE (NOM_CLASSE_COPIE ncc) autre instance de la classe Copie
    public Copie(Copie copie){
        int ageCopie = copie.age;
        String nomCopie = copie.nom;
    }

    public Copie(int ageNew , String nomNew){
        this.age = ageNew;
        this.nom = nomNew;
    }
}