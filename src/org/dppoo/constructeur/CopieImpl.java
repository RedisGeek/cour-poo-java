package org.dppoo.constructeur;

public class CopieImpl {
    public static void main(String[] args) {
        /*
        * Instance de la classe Copie
        * */
        Copie c = new Copie(12 , "SARAH");


        /*
        * Copie c
        * Copie d instance
        * */
        Copie c1 = new Copie(c);

        System.out.println(c1.getClass());

    }
}
