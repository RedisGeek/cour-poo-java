package org.dppoo.exo;

public class Boisson {

    private double TAUX_ALCOOL;
    private String NOM_BOISSON;
    private static final int ALCOOLIQUE = 0;
    private static final int HYGIENIQUE = 0;
    private static final int TAUX_NORMAL = 5;



    public Boisson(){

    }

    public double getTAUX_ALCOOL() {
        return TAUX_ALCOOL;
    }

    public String getNOM_BOISSON() {
        return NOM_BOISSON;
    }

    public static int getTauxNormal() {
        return TAUX_NORMAL;
    }

    public static int isALCOOLIQUE() {
        return ALCOOLIQUE;
    }

    public static int isHYGIENIQUE() {
        return HYGIENIQUE;
    }

    public void setTAUX_ALCOOL(double TAUX_ALCOOL) {
        this.TAUX_ALCOOL = TAUX_ALCOOL;
    }

    public void setNOM_BOISSON(String NOM_BOISSON) {
        this.NOM_BOISSON = NOM_BOISSON;
    }

    void setCategorie(){
        int compteur = 0;
        int i = compteur ++;
        if (getTAUX_ALCOOL() >  getTauxNormal()){
            System.out.println("Boisson Alcoolique ");
            System.out.println(" "+i+" Boisson Alcoolique");

        } else if (getTAUX_ALCOOL() < getTauxNormal()) {
            System.out.println("Boisson Hygienique");
            System.out.println(" " + i + " Boisson Hygienique");
        }
    }
}
