package org.dppoo.exo;

import java.util.Scanner;

public class BoissonImpl {

    public static void main(String[] args) {
        while (true){
            Scanner sc = new Scanner(System.in);
            Boisson b = new Boisson();

            System.out.println("Entrer le nom de la boisson : ");
            String nomBoisson = sc.next();
            System.out.println("Entrer le taux d alcool : ");
            double tauxAlcool = sc.nextDouble();

            b.setTAUX_ALCOOL(tauxAlcool);
            b.setNOM_BOISSON(nomBoisson);

            b.setCategorie();
        }
    }
}
