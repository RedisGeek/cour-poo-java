package org.dppoo.modificateur;

class PrivateClass {

    public PrivateClass(){

    }

    void afficheBanner(){
        System.out.println("***************");
        System.out.println("*** private ***");
        System.out.println("***************");
    }

    /*
    * Tsy afaka atsoina any @ Class hafa ny methode afficheBanner()
    * */
    public static void main(String[] args) {
        PrivateClass p = new PrivateClass();
        p.afficheBanner();
    }
}
