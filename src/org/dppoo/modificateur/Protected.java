package org.dppoo.modificateur;

class Protected {

    protected void afficheBanner(){
        System.out.println("*****************");
        System.out.println("*** protected ***");
        System.out.println("*****************");
    }

    public static void main(String[] args) {
        Protected pr = new Protected();
        pr.afficheBanner();
    }
}
