package org.dppoo.swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Fenetre extends JFrame{
    public Fenetre(){

        this.setSize(1000,1000);
        this.setLocationRelativeTo(null);
        this.setTitle("APPLICATION");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new BorderLayout());

        JPanel pan = new JPanel();
        pan.setBackground(Color.orange);
        this.setContentPane(pan);


        JLabel label = new JLabel();
        label.setText("VALEUR DANS TEXTFIELD");
        label.setVisible(true);

        JTextField textField = new JTextField(50);

        JButton btnOk = new JButton();
        /*btnOk.setSize(25,25);*/
        btnOk.setText("Afficher Valeur dans textfield");

        btnOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String nom = textField.getText();
                label.setText(nom);
            }
        });

        this.getContentPane().add(textField, BorderLayout.CENTER);
        this.getContentPane().add(btnOk, BorderLayout.NORTH);
        this.getContentPane().add(label, BorderLayout.EAST);
        this.setVisible(true);

    }
}
