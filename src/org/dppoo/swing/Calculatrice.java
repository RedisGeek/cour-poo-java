package org.dppoo.swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Calculatrice extends JFrame {

    private JPanel container = new JPanel();
    String caractere[] = {"1", "2", "3", "4", "5", "6", "7", "8",
            "9", "0", ".", "=", "C", "+", "-", "*", "/"};
    JButton[] btn = new JButton[caractere.length];

    private JLabel ecran = new JLabel();
    private Dimension dim = new Dimension(50,40);
    private Dimension dim2 = new Dimension(50,31);
    private double chiffre1;
    private boolean clicOperateur = false, update = false;
    private String operateur= " ";

    public Calculatrice(){
        this.setSize(250,260);
        this.setTitle("Calculatrice");
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        initComposant();
        this.setContentPane(container);
        this.setVisible(true);
    }

    private void initComposant(){
        Font police = new Font("Arial", Font.BOLD, 20);
        ecran = new JLabel("0");
        ecran.setFont(police);

        ecran.setHorizontalAlignment(JLabel.RIGHT);
        ecran.setPreferredSize(new Dimension(220,20));
        JPanel operateur = new JPanel();
        operateur.setPreferredSize(new Dimension(55,225));
        JPanel chiffre = new JPanel();
        chiffre.setPreferredSize(new Dimension(165,225));
        JPanel panEcran = new JPanel();
        panEcran.setPreferredSize(new Dimension(220,30));

        for (int i = 0; i < caractere.length; i++) {
            btn[i] = new JButton(caractere[i]);
            btn[i].setPreferredSize(dim);
            switch (i){
                case 11:
                    btn[i].addActionListener(new EgalListener());
                    chiffre.add(btn[i]);
                    break;
                case 12:
                    btn[i].setForeground(Color.red);
                    btn[i].addActionListener(new ResetListener());
                    operateur.add(btn[i]);
                    break;
                case 13:
                    btn[i].addActionListener(new PlusListener());
                    btn[i].setPreferredSize(dim2);
                    operateur.add(btn[i]);
                    break;
                case 14:
                    btn[i].addActionListener(new MoinListener());
                    btn[i].setPreferredSize(dim2);
                    operateur.add(btn[i]);
                    break;
                case 15:
                    btn[i].addActionListener(new MultiListener());
                    btn[i].setPreferredSize(dim2);
                    operateur.add(btn[i]);
                    break;
                case 16:
                    btn[i].addActionListener(new DivListener());
                    btn[i].setPreferredSize(dim2);
                    operateur.add(btn[i]);
                    break;
                default:
                    chiffre.add(btn[i]);
                    btn[i].addActionListener(new ChiffreListener());
                    break;
            }
        }
        panEcran.add(ecran);
        panEcran.setBorder(BorderFactory.createLineBorder(Color.black));
        container.add(panEcran, BorderLayout.NORTH);
        container.add(chiffre, BorderLayout.CENTER);
        container.add(operateur, BorderLayout.EAST);
    }

    private void Calcul(){
        if (operateur.equals("+")){
            chiffre1 = chiffre1 + Double.valueOf(ecran.getText());
            ecran.setText(String.valueOf(chiffre1));
        }
        if (operateur.equals("-")){
            chiffre1 = chiffre1 - Double.valueOf(ecran.getText()).doubleValue();
            ecran.setText(String.valueOf(chiffre1));
        }
        if (operateur.equals("*")){
            chiffre1 = chiffre1 * Double.valueOf(ecran.getText()).doubleValue();
            ecran.setText(String.valueOf(chiffre1));
        }
        if (operateur.equals("/")){
            try {
                chiffre1 = chiffre1 / Double.valueOf(ecran.getText()).doubleValue();
                ecran.setText(String.valueOf(chiffre1));
            } catch (ArithmeticException e) {
                ecran.setText("0");
                e.printStackTrace();
            }
        }
    }

    class ChiffreListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            String str = ((JButton)actionEvent.getSource()).getText();
            if (update){
                update = false;
            } else {
                if (!ecran.getText().equals("O"))
                    str = ecran.getText() + str;
            }
            ecran.setText(str);
        }
    }

    class DivListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (clicOperateur){
                Calcul();
                ecran.setText(String.valueOf(chiffre1));
            } else {
                chiffre1 = Double.valueOf(ecran.getText()).doubleValue();
                clicOperateur = true;
            }
            operateur = "/";
            update = true;
        }
    }

    class MultiListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (clicOperateur){
                Calcul();
                ecran.setText(String.valueOf(chiffre1));
            } else {
                chiffre1 = Double.valueOf(ecran.getText()).doubleValue();
                clicOperateur = true;
            }
            operateur = "*";
            update = true;
        }
    }

    class MoinListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (clicOperateur){
                Calcul();
                ecran.setText(String.valueOf(chiffre1));
            } else {
                chiffre1 = Double.valueOf(ecran.getText()).doubleValue();
                clicOperateur = true;
            }
            operateur = "-";
            update = true;
        }
    }

    class PlusListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (clicOperateur){
                Calcul();
                ecran.setText(String.valueOf(chiffre1));
            } else {
                chiffre1 = Double.valueOf(ecran.getText()).doubleValue();
                clicOperateur = true;
            }
            operateur = "+";
            update = true;
        }
    }

    class ResetListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            clicOperateur = false;
            update = true;
            chiffre1 = 0;
            operateur= "";
            ecran.setText("");
        }
    }

    class EgalListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            Calcul();
            update = true;
            clicOperateur = false;
        }
    }

    public static void main(String[] args) {
        Calculatrice calc = new Calculatrice();
    }
}
