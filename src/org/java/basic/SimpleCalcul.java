package org.java.basic;

public class SimpleCalcul {
    public static void main(String[] args) {
        double nbr1 = 10;
        double nbr2 = 3;

        // normalement misy virgule ny resultat fa natao conversion (cast) (int) io
        int resultat = (int) (nbr1 / nbr2);

        // mi-retourne double 3.33
        //double resultat = nbr1 / nbr2;

        System.out.println("resultat : "+resultat);
    }
}
