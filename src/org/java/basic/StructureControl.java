package org.java.basic;

import java.util.Scanner;

public class StructureControl {
    /*
    * Structure de controle
    *   - boucle
    *   - condition
    *   - instruction
    *
    * BOUCLE :
    *   while
    *   do while
    *   for
    *
    * STRUCTURE
    *   if .. else
    *   if else if
    *   if embrique
    *   switch .. case
    *
    * --> DECOMMENTEZ LES CODE POUR TESTER !!
    *
    * */

    public static void main(String[] args) {

        /*
        * Condition
        *   structure IF ELSE
        * */
        Scanner sc = new Scanner(System.in);

        /*final int LIMIT_AGE = 18;

        Scanner sc = new Scanner(System.in);
        System.out.println("Entrer votre age : ");
        int age = sc.nextInt();
        sc.nextLine();*/

        // Simple if ... else
        /*if (age == LIMIT_AGE){
            System.out.println("vous etes majeur");
        } else {
            System.out.println("vous etes mineur");
        }*/

        // if ... else if ...
        // if embrique
        /*if (age == LIMIT_AGE){
            System.out.println("vous etes majeur");
        } else if (age <= 15){
            System.out.println("un(e) gamin(e)");
        } else {
            System.out.println("Enfant");
        }*/

        /*System.out.println("Entrer votre note : ");
        int note = sc.nextInt();
        sc.nextLine();*/

        //  Structure Switch
        /*switch (note){
            case 0:
                System.out.println("Mauvais eleve");
                break;
            case 5:
                System.out.println("Mauvais note");
                break;
            case 10:
                System.out.println("Moyenne");
                break;
            case 15:
                System.out.println("Bonne note");
                break;
            case 18:
                System.out.println("Tres bonne note");
                break;
            default:
                System.out.println("un peu d effort please !!");
        }*/

        // Condition ternaires

        /*int x = 10;
        int y = 20;

        int res = (x < y) ? x : y;
        System.out.println(res);*/

        // Condition Multiple



    }
}
