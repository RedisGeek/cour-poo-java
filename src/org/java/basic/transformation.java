package org.java.basic;

public class transformation {
    /*
    * Transformation argument Type de donnee
    * ex : int en String
    * Consulter JAVA DOC API pour les methode valueOf , intValue , ...
    * */
    public static void main(String[] args) {

        // entier 10
        int x = 10;

        // j String
        String j = new String();

        // eto ny j lasa String mi-contenir caractere 12
        j = j.valueOf(x);

        int a = 1;
        // mi retourne 110 , satri ny valeur an J efa considereny ho caractere fa tsy entier intsony
        System.out.println(a + j);

        // mamerina ny x ho entier
        int k = Integer.valueOf(x).intValue();
        System.out.println(k+a);
    }
}
