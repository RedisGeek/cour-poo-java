package org.java.basic;

import java.util.Scanner;

public class EntreeSortie {

    public static void main(String[] args) {

        /*
        * Entree / Sortie en java
        *
        * Mba ahfahana mamaky izay apidirina @ clavier @ java dia mapiasa objet de type Scanner
        * */

        // Objet de type Scanner , mi-prendre parametre System.in (Entree standard)
        Scanner sc = new Scanner(System.in);

        System.out.println("entrer votre age : ");
        int age = sc.nextInt();
        /*
            int i = sc.nextInt();
            double d = sc.nextDouble();
            long l = sc.nextLong();
            byte b = sc.nextByte();
        */
        System.out.println("Entrer votre nom : ");
        String nom = sc.next();

        //Recuperation caractere

        // Retourne 2eme caractere dans le nom
        char caractere = nom.charAt(1);

        System.out.println("caractere : "+caractere);

        System.out.println("Vous etes "+nom+" , "+age+" ans");
    }
}
