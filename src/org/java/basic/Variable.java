package org.java.basic;

public class Variable {
    public static void main(String[] args) {
        System.out.println("LES VARIABLE EN JAVA ");
        /*
        * Variable :
        *   manana nom , type , valeur
        * Type primitif (type simple)
        * Type Objet
        *
        * Type elementaire
        *   int : entier
        *   boolean : booleen true na false , tsy mety atao conversion de type
        *   char : caractere ex: a , b , c ...
        *   String : chaine de caractere (mots , anarana , ....)
        *   float : nombre a virgule flotante simple (45.5 , 12.3 )
        *   double : nombre a virgule flotante double (0.5 , 2.1 )
        *   long : entier long
        *   byte : octet signe
        *   short : entier court signe
        *
        * Initialisation variable : TYPE_VARIABLE nom = valeur;
        *
        * */

        /*
        * byte : 1 octet
        * int : 4 octet
        * float : 4 octet
        * short : 2 octet
        * Long : 8 octet
        * double : 8 octet
        * */

        // Entier int
        int age = 12;
        // booleen valeur logique
        boolean isTrue = false;
        // char caractere
        char alphabet = 'a';
        //String
        String nom = "Deepnisha Ramphul";
        //float (declaration float : float nom_variable = valeur + f , ex: float surface = 1.34f)
        float pi = 3.14f;
        //double (d a la fin)
        double aire = 2.222222222222222d;
        // long (L a la fin , mi-informer an le JVM hoe hapiasa long)
        long cin = 101231162890L;
        //byte
        byte ADDR = 0xf;
        //short
        short s = -5;

        /*
        * Affichage utilisant System.out.println();
        * + : concatenation (ex: "text " + value1 + "text ...")
        * */

        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("INT : "+age);
        System.out.println("BOOLEAN : "+isTrue);
        System.out.println("CHAR : "+alphabet);
        System.out.println("STRING : "+nom);
        System.out.println("FLOAT : "+pi);
        System.out.println("DOUBLE : "+aire);
        System.out.println("LONG : "+cin);
        System.out.println("BYTE : "+ADDR);
        System.out.println("SHORT : "+s);
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");

        // Affectation
        int a = 20;
        int c = a;
        c = a;
        c = a + 1;

        System.out.println("Affectation a dans c : "+c);


        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");

        // conversion type , cast
        short x = 4;
        short y = 5;
        short res = (short) (x + y);
        // Erreur de compilation : short res = x + y;
        System.out.println("resultat : " +res);

        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");

        int litreDeau = 1;
        // Resultat : 1.0 , satri efa nisy conversion (Cast) (float) sy (double) io
        System.out.println("CAST INT EN FLOAT : "+ (float) litreDeau);
        System.out.println("CAST INT EN DOUBLE : "+ (double) litreDeau);
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");

        /*
        *  Incrementation sy decrementation
        * ex: x++ , i++ , ++x , --x , x--
        * ++x , x++ equivalent a x = x + 1
        *
        * */
        int val1 = 0;
        // ou
        //val1 = val1 + 1;

        // si val1++ ca marche pas

        //resultat = 1
        System.out.println("INCREMENTATION : "+ ++val1);

        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");


    }
}
