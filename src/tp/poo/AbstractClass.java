package tp.poo;

abstract class AbstractClass {


    /*
    * Void : tsy misy retour (void = rien)
    * methode crier() sans parametre
    * Syntaxe : MODIFICATEUR_D_ACCES Type_de_retour (raha misy) nom_methode(Param1 , param2 <raha misy>)
    *
    * */
    void crier(){
        System.out.println("ABSTRACT CRIER : AAAAAAAAAAAAAAAAAAAAAAAAAAAAhhhhhh  !!! XD");
    }

    void silence(){
        System.out.println("ABSTRACT SILENCE : shuuuuut !! :| ");
    }
}