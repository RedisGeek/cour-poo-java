package tp.poo;

import java.util.Scanner;

/*
* Exemple 1
* Class Maison
* Modificateur d acces (porte) : public
* Declaration : public class NOM_CLASSE
*   porte
*       PUBLIC : Accessible partout
*       ABSTRACT : classe mi-contenir methode abstraite , tsy afaka instanciena direct
*       FINAL : Tsy afaka modifiena , tsy manana classe fille
*
* Syntaxe : modificateur class NOM_CLASSE (extends CLASSE_MERE rah misy) (implements INTERFACE rah misy)
* */
public class Maison {

    /*
    * Attribut
    * Variable d instance
    * Declaration : portee type NOM_VARIABLE
    * porte : Ahfahana mi-definir hoe hatraiza no azo ampiasana an ilay variable
    * PUBLIC : Accessible partout
    * PRIVATE : Accessible ao @ ilay package ihany
    * PROTECTED : Accessible ao @ ilay classe ihany
    *
    * */
    private int nbrChambre;
    private String nomChambre;


    /*
    * Constructeur par defaut , sans parametre
    * */
    public Maison(){

    }

    void ajoutColoc(int numChambre , String nameChambre){
        this.nbrChambre = numChambre;
        this.nomChambre = nameChambre;

        System.out.println("Ajout coloc : ");
        Scanner sc = new Scanner(System.in);
        System.out.println("NOM DU COLOC :");
        String nomColoc = sc.next();

        System.out.println("Coloc : "+nomColoc+" Ajouter au chambre : "+numChambre+" dans : "+nameChambre);
    }
}
