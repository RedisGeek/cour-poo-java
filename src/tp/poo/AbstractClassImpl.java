package tp.poo;

public class AbstractClassImpl extends AbstractClass {
    AbstractClass abstractClass = new AbstractClass() {
        @Override
        void crier() {
            super.crier();
        }

        @Override
        void silence() {
            super.silence();
        }
    };
}
