package tp.java.poo;

/*
*   Declaration classe en java
*   MODIFICATEUR D'ACCESS, mot-cle class, NOM DU CLASSE {}
*
* */
public class Rectangle {

    /*
     * Attribut du classe rectangle
     * Declaration attribut : TYPE nom_de_l_attribut;
     *
     * Tsy afaka ovaina , satria tsy manana avela manana access direct @ny attribut ana classe ny utilisateur
     */
    double longeur = 2;
    double largeur = 3;

    /*
    * Declaration Methode :
    * TYPE nom_methode(type_param param1, type_param param2){
    *   code ici ...
    * }
    *
    * Sans parametre satria  tsy nomena access hanova  ny attribut ny utilisateur
    *
     */
    double surface(){
        return longeur * largeur;
    }

    public static void main(String[] args) {
        Rectangle r = new Rectangle();
        double surface = r.surface();
        System.out.println("Surface : "+surface);
    }
}
